FROM ubuntu:22.04

RUN useradd -ms /bin/bash user
WORKDIR /home/user
COPY main .
RUN chmod +x main
USER user
